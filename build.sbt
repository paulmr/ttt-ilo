enablePlugins(SbtTwirl)

scalacOptions += "-feature"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "4.0.0-RC2",
  "commons-io" % "commons-io" % "2.6",
  "org.clapper" %% "markwrap" % "1.2.0"
)
