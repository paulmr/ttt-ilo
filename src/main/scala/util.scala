package tttilo

import java.io.File
import java.nio.file.Path

object utils {

  import scala.language.implicitConversions

  implicit def autoPath(f: File): Path = f.toPath()
  implicit def autoFile(s: String): File = new File(s)
}
