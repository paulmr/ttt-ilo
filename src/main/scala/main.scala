package tttilo

import org.apache.commons.io.FileUtils
import scopt.OParser
import java.io.File

case class Agordoj(
  fontujo: File = new File("ekzemplo"),
  rezultujo: File = new File("rezulto"),
  konservuRezultujon: Boolean = false,
  parolema: Boolean = true
)

object Main {

  def resolve(f: File) =
    new File(f.toString().replaceAll("~", System.getenv("HOME")))

  def main(argumentoj: Array[String]): Unit = {
    val konstruilo = OParser.builder[Agordoj]
    val skandilo = {
      import konstruilo._
      OParser.sequence(
        programName("tttilo"),
        help("helpi").text("montri ĉi tiun helpotekston"),
        opt[File]('f', "fonto")
          .action((f, a) => a.copy(fontujo = resolve(f)))
          .text("difini la fonto-dosierujon"),
        opt[File]('r', "rezulto")
          .action((r, a) => a.copy(rezultujo = resolve(r)))
          .text("difini la rezulto-dosierujon"),
        opt[Unit]('k', "konservu-rezultujon")
          .action((_, a) => a.copy(konservuRezultujon = true))
          .text("Ne forviŝu la rezultujon antaŭ ol komenci"),
        opt[Unit]('p', "parolema")
          .action((_, a) => a.copy(parolema = true))
          .text("indiki ke ni detale listigu le rezultojn, ke ne nur la problemojn")
      )
    }
    val agordoj = OParser.parse(skandilo, argumentoj, Agordoj()).getOrElse {
      throw new IllegalArgumentException("malbonaj agordoj")
    }

    if(!agordoj.konservuRezultujon) {
      FileUtils.deleteDirectory(agordoj.rezultujo)
      if(agordoj.parolema) println(s"Forigis rezultujon (${agordoj.rezultujo})")
    }

    val retejoProduktilo = new SiteGenerator(agordoj.fontujo, agordoj.rezultujo, agordoj.parolema)
    retejoProduktilo.generate()
  }
}
