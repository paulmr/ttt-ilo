package tttilo

import scala.util.{ Try, Success, Failure }

import org.apache.commons.io.FileUtils
import java.io.{ File, FileOutputStream }

import scala.io.Source
import play.twirl.api.Html

import org.clapper.markwrap.{ MarkWrap, MarkWrapParser }

import utils._
import scala.util.Failure
import java.io.FileNotFoundException

trait Processor {
  val baseDir: File
  val targetDir: File

  def outputFile(inputFile: File, createDir: Boolean = true): File = {
    val res = targetDir.resolve(baseDir.relativize(inputFile)).toFile
    if(createDir) new File(res.getParent()).mkdirs()
    res
  }

  def process(inputFile: File): Try[File]
}

class FileCopy(val baseDir: File, val targetDir: File) extends Processor {
  def process(inputFile: File) = Try {
    val of = outputFile(inputFile)
    FileUtils.copyFile(inputFile, of)
    of
  }
}

trait FilterProcessor extends Processor {
  def template: Template
  def filter(input: String): String
  def process(inputFile: File) = Try {
    val input = Source.fromFile(inputFile).mkString
    val output = template("title here", Html(filter(input))).toString
    val outputF = outputFile(inputFile)
    new FileOutputStream(outputF).write(output.getBytes())
    outputF
  }
}

class MarkWrapProcessor(parser: MarkWrapParser, val baseDir: File, val targetDir: File, val template: Template) extends FilterProcessor {

  override def outputFile(inputFile: File, createDir: Boolean = true): File =
    new File(super.outputFile(inputFile, createDir).toString.replaceFirst("\\.[^.]+$", ".html"))

  def filter(input: String): String = parser.parseToHTML(input)
}

trait FileGenerator {
  def processFiles(): List[Try[File]]
}

trait DirFileGenerator extends FileGenerator {
  val baseDir: File
  val targetDir: File
  val processors: ProcMapping

  require(baseDir.exists(), s"$baseDir ne ekzistas")

  def findFiles(): List[File] = {
    baseDir.listFiles().toList
  }

  def processFile(f: File): Try[File] = processors(f.toString).process(f)

  def processFiles(): List[Try[File]] = for(f <- findFiles()) yield(processFile(f))

}

class PageGenerator(val baseDir: File, val targetDir: File) extends DirFileGenerator {
  val processors =
    (Proc {
      case MarkFile(converter) => new MarkWrapProcessor(converter, baseDir, targetDir, html.page.apply)
    }) orElse (Proc {
      case _ => new FileCopy(baseDir, targetDir)
    })
}

class AssetsGenerator(val baseDir: File, val targetDir: File) extends DirFileGenerator {
  val processors = Proc {
    case _ => new FileCopy(baseDir, targetDir)
  }
}

class ResourceGenerator(files: Map[String, File], val targetDir: File) extends FileGenerator {

  def processFiles(): List[Try[File]] = files.map {
    case (resource, targ) => Try {
      val in = getClass().getResourceAsStream(resource)
      if(in == null) throw new IllegalArgumentException(s"Missing resource: $resource")
      val outFile = targetDir.resolve(targ).toFile
      FileUtils.copyInputStreamToFile(in, outFile)
      outFile
    }
  }.toList

}
