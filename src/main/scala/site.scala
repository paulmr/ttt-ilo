package tttilo

import java.io.File
import scala.util.{ Success, Failure }
class SiteGenerator(baseDir: File, targetDir: File, verbose: Boolean = true) {

  import utils._

  def generate(): Unit = {
    if(!targetDir.exists()) {
      targetDir.mkdir()
    }

    val stokujo = targetDir.resolve("stoko").toFile

    val generators = List(
      new PageGenerator(baseDir.resolve("paĝoj").toFile, targetDir.resolve("paĝoj").toFile),
      new AssetsGenerator(baseDir.resolve("stoko").toFile, stokujo),
      new ResourceGenerator(Map("/style.css" -> new File("style.css")), stokujo)
    )

    val res = generators.flatMap(_.processFiles())

    val (successes, failures) = res.partition(_.isSuccess)

    if(verbose) {
      println("Produktitaj dosieroj:")
      successes.foreach { case Success(fname) => println(s"\t+ $fname"); case _ => () }
    }

    failures foreach { case Failure(err) => println(err); case _ => () }
  }
}
