import play.twirl.api.Html
import scala.util.matching.Regex
import java.io.File
import org.clapper.markwrap.{ MarkWrap, MarkWrapParser }

package object tttilo {
  type Template = (String, Html) => Html
  type ProcMapping = PartialFunction[String, Processor]

  // tiu ĉi `unapply`-metodo, sukcesas se ekzistas konvertilo por la nomo de la dosiero
  object MarkFile {
    def unapply(fname: String): Option[MarkWrapParser] = MarkWrap.converterFor(new File(fname)).toOption
  }

  def Proc(pf: ProcMapping) = pf

  def Re(re: Regex)(proc: => Processor) = Proc {
    case re() => proc
  }
}
